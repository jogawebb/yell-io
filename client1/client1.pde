//Import the relevant libraries. 
import netP5.*;
import oscP5.*;
import processing.sound.*; 

Sound soundCard;
AudioIn in1, in2;
Amplitude amp1, amp2; 
FFT fft1, fft2;

OscP5 oscp5; 
NetAddress myServerLocation; 

int i = 0; 
int bands = 256; 
float freq1, freq2; 
float a1, a2, f1, f2, a1Round, a2Round; 
float[] spectrum1 = new float[bands];
float[] spectrum2 = new float[bands]; 

void setup() {
  size(1024, 720); 
  
  soundCard = new Sound(this);
  soundCard.sampleRate(22050);

  myServerLocation = new NetAddress("127.0.0.1", 12000);
  oscp5 = new OscP5(this, 12001); 
  OscProperties properties = new OscProperties(); 
  properties.setDatagramSize(512); 

  //Always check the input device to make sure it's the proper source. 
  soundCard.inputDevice(7);

  in1 = new AudioIn(this, 0); 
  in2 = new AudioIn(this, 1); 
  amp1 = new Amplitude(this); 
  amp2 = new Amplitude(this);
  fft1 = new FFT(this, bands); 
  fft2 = new FFT(this, bands); 

  in1.start(); 
  in2.start();
  amp1.input(in1); 
  amp2.input(in2); 
  fft1.input(in1); 
  fft2.input(in2);
}

void draw() { 
  background(0); 
  a1 = amp1.analyze(); 
  a2 = amp2.analyze(); 
  fft1.analyze(spectrum1); 
  fft2.analyze(spectrum2); 
  a1Round = round(a1 * 10000) * 0.0001; 
  a2Round = round(a2 * 10000) * 0.0001;

  OscMessage soundMessageOne = new OscMessage("/clientAmp"); 
  soundMessageOne.add("NORTH"); 
  soundMessageOne.add(a1Round); 
  oscp5.send(soundMessageOne, myServerLocation); 
  soundMessageOne.clear(); 

  OscMessage soundMessageTwo = new OscMessage("/clientAmp"); 
  soundMessageTwo.add("EAST"); 
  soundMessageTwo.add(a2Round); 
  oscp5.send(soundMessageTwo, myServerLocation); 
  soundMessageTwo.clear(); 

  //uncomment the next two lines to hear the inputs. Make sure you have headphones in!!!
  //in1.play(); 
  //in2.play(); 

  for (int i = 0; i < bands; i++) {
    if (spectrum1[i] == max(spectrum1)) {
      freq1 = i * ((22050 / 2.0) / bands); 
      println(freq1); 
    }
    if (spectrum2[i] == max(spectrum2)) {
      freq2 = i * ((22050 / 2.0) / bands); 
      println(freq2); 
    }
  }
  
  for (int i = 0; i < bands; i++) {
    stroke(0, 255, 0); 
    line(i * 4, height, i * 4, height - spectrum1[i] * height * 40); 
    line((i * 4 + 256), height, (i * 4+ 256), height - spectrum2[i] * height * 40);
    if (spectrum1[i] == max(spectrum1)) {
      text(i * ((22050 / 2.0) / bands), 50, 50); 
    }
  }
  
  OscMessage freqMessageOne = new OscMessage("/clientFreq"); 
  freqMessageOne.add("FNORTH"); 
  freqMessageOne.add(freq1); 
  oscp5.send(freqMessageOne, myServerLocation);
  freqMessageOne.clear(); 
  
  OscMessage freqMessageTwo = new OscMessage("/clientFreq"); 
  freqMessageTwo.add("FEAST"); 
  freqMessageTwo.add(freq1); 
  oscp5.send(freqMessageTwo, myServerLocation);
  freqMessageTwo.clear(); 
}
