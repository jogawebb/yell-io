class Ball {
  float xpos, ypos;
  float size;
  float velocityX = 0, velocityY = 0;

  Ball (float ballSize) {
    xpos = width/2;
    ypos = height/2;
    size = ballSize;
  }
  
  void drawBall () {
    fill(255,255,255);
    ellipse (xpos, ypos, size, size);
  }
  
  void updateSpeed() {
    this.velocityY += a * player[0].moving * 100;
    this.velocityX -= a * player[1].moving * 100;
    this.velocityY -= a * player[2].moving * 100;
    this.velocityX += a * player[3].moving * 100;
    
    // Update location.
    this.xpos = min(max(this.xpos + this.velocityX, this.size / 2), width - (this.size / 2)); 
    this.ypos = min(max(this.ypos + this.velocityY, this.size / 2), height - (this.size / 2));
    // Decrease speed due to traction.
    this.velocityX *= 0.99;
    this.velocityY *= 0.99; 
  } 
  
  void resetBall() {
    xpos = width/2;
    ypos = height/2;
  }
}
