class Player {
  float length;
  String position;
  float area;
  float moving = 0;
  float frequencies[]; 
  float totalFrequency;
  int readIndex = 0;

  Player (String position) {
    this.position = position;
    this.length = width/4;
    frequencies = new float[nrReadings];
    for(int i = 0; i < nrReadings; i++) {
      this.frequencies[i] = 0;
    }
  }
  
  void updateFrequencies(float frequency) {
    this.totalFrequency = this.totalFrequency - this.frequencies[readIndex];
    // read from the sensor:
    this.frequencies[readIndex] = frequency;
    // add the reading to the total:
    this.totalFrequency = this.totalFrequency + frequencies[readIndex];
    // advance to the next position in the array:
    readIndex = (readIndex + 1) % nrReadings;
    this.length = height/2 - (max(0, abs(totalFrequency/nrReadings - goalFreq))/2);
    println("length = " + this.length);
  }
  
  void drawPlayer() {
    switch(position) {
      case "left": 
        fill(255, 0, 0);
        drawTriangle(0, height/2 - this.length, 0, height/2 + this.length, ball.xpos, ball.ypos);
        break;
      case "right": 
        fill(0, 255, 0);
        drawTriangle(width, height/2 - this.length, width, height/2 + this.length, ball.xpos, ball.ypos);
        break;
      case "up":
        fill(0, 0, 255);
        drawTriangle(width/2 - this.length, 0, width/2 + this.length, 0, ball.xpos, ball.ypos);
        break;
      case "down":
        fill(255,255,255);
        drawTriangle(width/2 - this.length, height, width/2 + this.length, height, ball.xpos, ball.ypos);
        break;
    }
  }
  
  void drawTriangle(float x1, float y1, float x2, float y2, float x3, float y3) {
    triangle(x1, y1, x2, y2, x3, y3);
    this.area = abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2))/2);
    println(this.position + " " +  this.area);
    
  }
  
}
