//Import libraries
import processing.sound.*; 

import oscP5.*;
import netP5.*;

//Instantiate the audio elements. 
Sound soundCard; 
AudioIn in1, in2; 
Amplitude amp1, amp2; 
SinOsc sine; 

OscP5 oscP5;
OscProperties properties; 

//Handle the inputs from the microphones
float aN, aS, aE, aW; 
float[] frequencies = {0,0,0,0};
//float fN, fS, fE, fW; 

//import font 
PFont myFont; 

Player player[];
// Up means sound FROM upside direction not towards]
String directions[] = {"up", "right", "down", "left"};
String colors[] = {"Blue", "Green", "White", "Red"}; 
Ball ball;
int[] scores = {0,0,0,0};
float scoreTime = 8000, currentTime, gameTime = 80001, startTime;
int goalSize = 150;
int playerSize = 5;
float a = 0.14;
int nrReadings = 100;
float goalFreq;
float thresholdAmp = 0.01; //formerly thresholdAmp.
float limitAmp = 0.3; //

void setup () {
  
  myFont = createFont("VT323 Regular", 48); 
  textFont(myFont, 128);  
  
  Sound.list(); 
  
  sine = new SinOsc(this); 
  
  oscP5 = new OscP5(this, 12000);
  OscProperties properties = new OscProperties(); 
  properties.setDatagramSize(512); 
  
  exec("/home/gabe/Yell-IO/client1/application.linux64/client1"); 
  exec("/home/gabe/Yell-IO/client2/application.linux64/client2"); 
  
  delay(5000);
  
  size(1600, 1600);
  noStroke();
  player = new Player[4];
  for(int i = 0; i < 4; i++){
    player[i] = new Player(directions[i]);
  }
  ball= new Ball(goalSize);
  goalFreq = random(200, 500);
  sine.freq(goalFreq); 
  sine.play(); 
  currentTime = millis();
  startTime = millis();
}

void draw () {

  background(0);
  if(millis() - startTime > gameTime) {
    sine.stop(); 
    int maxIndex = 0;
    int maxScore = 0;
    for(int i=0 ; i<4; i++) {
      if (scores[i] > maxScore) {
        maxScore = scores[i];
        maxIndex = i;
      }
    }
    fill(255);
    text("Congratulations player " + colors[maxIndex] +  "!",  width / 2 - 200, height/2 - 200); 
    for(int i = 0; i<4; i++) {
      text("Player " + colors[i] +  ": " + scores[i] + " points",  width / 2 - 200, height/2 + ((i+1)*100) - 200); 
    } 
    
    text("Press [ENTER] to restart.", width / 2 - 200, height - 400); 
    
  } else {
    if(millis() - currentTime > scoreTime) {
      currentTime = millis();
      handleScoring();
      goalFreq = random(200, 500);
      sine.freq(goalFreq); 
      sine.play(); 
    }
    micCheck();
    ball.updateSpeed();
  
    String scoreString = "";
    for(int i = 0; i < 4; i++){
      player[i].updateFrequencies(frequencies[i]);
      //player[i].length = random(width/4); // test
      player[i].drawPlayer();
      //player[i].calcSpeed();
      scoreString = scoreString + " " + directions[i] + ": " + scores[i];
    }
    fill(255,255,255);
    
    fill(0);
    textSize(120);  
    text(scores[0],  width / 2, 90); 
    text(scores[1], width - 70, height / 2); 
    text(scores[2], width / 2, height - 70); 
    text(scores[3], 50, height / 2); 
   
    ball.drawBall();
    fill(0);
    textSize(52); 
    text(int(goalFreq) + " Hz", ball.xpos - 60, ball.ypos - 20, 200, 200); 
  }
}


//void keyPressed() {
//  if (keyCode == UP) {
//    player[0].moving = true;
//  } else if (keyCode == DOWN) {
//    player[2].moving = true;
//  } else if(keyCode == LEFT) {
//    player[3].moving = true;
//  } else if(keyCode == RIGHT) {
//    player[1].moving = true;
//  }
//} 


//void keyReleased () {
//  if (keyCode == UP) {
//    player[0].moving = false;
//  } else if (keyCode == DOWN) {
//    player[2].moving = false;
//  } else if(keyCode == LEFT) {
//    player[3].moving = false;
//  } else if(keyCode == RIGHT) {
//    player[1].moving = false;
//  }
//} 

void oscEvent(OscMessage incomingMessage) {
  if (incomingMessage.checkAddrPattern("/clientAmp") == true) {
    String microphone = incomingMessage.get(0).stringValue(); 
    if (microphone.equals("NORTH")) {
      aN = incomingMessage.get(1).floatValue(); 
      //println("aN: " + aN); 
    }
    if (microphone.equals("EAST")) {
      aE = incomingMessage.get(1).floatValue(); 
      //println("aE: " + aE); 
    }
    if (microphone.equals("SOUTH")) {
      aS = incomingMessage.get(1).floatValue(); 
      //println("aS: " + aS); 
    }
    if (microphone.equals("WEST")) {
      aW = incomingMessage.get(1).floatValue(); 
      //println("aW: " + aW); 
    }
  }
  if (incomingMessage.checkAddrPattern("/clientFreq") == true) {
    String microphone = incomingMessage.get(0).stringValue();
    if (microphone.equals("FNORTH")) {
      frequencies[0] = incomingMessage.get(1).floatValue(); 
      println("fN: " + frequencies[0]); 
    }
    if (microphone.equals("FEAST")) {
      frequencies[1] = incomingMessage.get(1).floatValue(); 
      println("fE: " + frequencies[1]); 
    }
    if (microphone.equals("FSOUTH")) {
      frequencies[2] = incomingMessage.get(1).floatValue(); 
      println("fS: " + frequencies[2]); 
    }
    if (microphone.equals("FWEST")) {
      frequencies[3] = incomingMessage.get(1).floatValue(); 
      println("fW: " + frequencies[3]); 
    }
  }
  incomingMessage.clear(); 
}

void micCheck(){
  if (aN > thresholdAmp && aN < limitAmp) {
    player[0].moving = aN; 
  }
  if (aE > thresholdAmp && aE < limitAmp) {
    player[1].moving = aE; 
  }
  if (aS > thresholdAmp && aS < limitAmp) {
    player[2].moving = aS; 
  }
  if (aW > thresholdAmp && aW < limitAmp) {
    player[3].moving = aW; 
  }
}
  
 void handleScoring() {
   float maxArea = 0;
   int playerID = 0;
   for(int i=0; i<4; i++) {
     if(player[i].area > maxArea) {
       maxArea = player[i].area;
       playerID = i;
     }
   }
   scores[playerID]++;
 }
 
 void resetGame() {
   ball.resetBall(); 
   for (int i = 0; i < 4; i++) {
     scores[i] = 0; 
   }
   goalFreq = random(200, 500);
   sine.freq(goalFreq); 
   sine.play();
   currentTime = millis();
   startTime = millis();
 }
 
 void keyPressed() {
   if (key == ENTER) {
     resetGame(); 
   }
 }
