class Goal {
  float xpos, ypos;
  float size;
  

  Goal (float goalSize) {
    xpos = random(goalSize, width-goalSize);
    ypos = random(goalSize, height-goalSize);
    size = goalSize;
  }
  void drawGoal () {

    ellipse (xpos, ypos, size, size);
  }
  
  void resetGoal() {
    xpos = random(goalSize, width-goalSize);
    ypos = random(goalSize, height-goalSize);
  }
}
