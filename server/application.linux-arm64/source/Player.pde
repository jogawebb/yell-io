class Player {
  float xpos, ypos;
  float size;
  Boolean up = false, down = false, right = false, left = false;
  float velocityX = 0, velocityY = 0;

  Player (float playerSize) {
    xpos = random(playerSize, width-playerSize);
    ypos = random(playerSize, height-playerSize);
    size = playerSize;
  }
  
  void updateSpeed() {
    if (this.left ) { this.velocityX -= a; }
    if (this.right) { this.velocityX += a ; }
    if (this.up) { this.velocityY -= a ;}
    if (this.down) { this.velocityY += a ;}
    
    // Update location.
    this.xpos = (width + this.xpos + this.velocityX) % width;
    this.ypos = (height + this.ypos + this.velocityY) % height;
    // Decrease speed due to traction.
    this.velocityX *= 0.99;
    this.velocityY *= 0.99;
  }
  
  void drawPlayer () {
    fill(0, 255, 0); 
    ellipse (this.xpos, this.ypos, this.size, this.size);
  }
  Boolean checkOverlap() {
    return dist(this.xpos, this.ypos, goal.xpos, goal.ypos) <= goal.size/2;
  }
}
