import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.sound.*; 
import controlP5.*; 
import oscP5.*; 
import netP5.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class server extends PApplet {

//Import the relevant libraries. 






//import the font
PFont myFont; 

//Instantiate the audio elements from the Sound library. 
Sound soundCard; 
AudioIn in1, in2; 
Amplitude amp1, amp2; 

//Instantiate osc elements for server-client communication. 
OscP5 oscP5; 
OscProperties properties;  

//Instantiate the game elements from the classes below. 
Player player;
Goal goal;

int score = 0;
int goalSize = 150;
int playerSize = 30;
float a = 0.14f;

int currentTime, startTime; 

float aN, aS, aE, aW; 
float fN, fS, fE, fW; 

public void setup() {
  Sound.list(); 
  
  myFont = createFont("VT323 Regular", 128); 
  textFont(myFont, 128); 
  
  oscP5 = new OscP5(this, 12000);
  OscProperties properties = new OscProperties(); 
  properties.setDatagramSize(512); 
  
  
  goal = new Goal(goalSize);
  player = new Player(playerSize);
  
  noStroke();
  
  startTime = millis();
  
  exec("/home/gabe/Yell-IO/client1/application.linux64/client1"); 
  exec("/home/gabe/Yell-IO/client2/application.linux64/client2"); 
}

public void draw() {

  background(25); 
  fill(255); 

  micCheck(); 
  goal.drawGoal();
  player.drawPlayer();
  player.updateSpeed();

  if (player.checkOverlap()) {
    score++;
    goal.resetGoal();
  }
  textSize(100);
  fill(255); 
  text("Score: " + score, 100, 100); 
  
  currentTime = millis(); 
  int clockTime = currentTime - startTime; 
  
  textSize(100); 
  fill(255); 
  text("Time: " + (60 - (clockTime / 1000)), 800, 100); 

  if (clockTime >= 60000) {
    background(0); 
    text("Congratulations! You scored " + score, width / 4, height / 4, width / 2 + 100, height / 2); 
    text("Press [ENTER] to restart.", width / 4, height / 4 + 500, width / 2, height / 2); 
  
  }
}

public void oscEvent(OscMessage incomingMessage) {
  if (incomingMessage.checkAddrPattern("/clientAmp") == true) {
    String microphone = incomingMessage.get(0).stringValue(); 
    if (microphone.equals("NORTH")) {
      aN = incomingMessage.get(1).floatValue(); 
      println("aN: " + aN);
    }
    if (microphone.equals("EAST")) {
      aE = incomingMessage.get(1).floatValue(); 
      println("aE: " + aE);
    }
    if (microphone.equals("SOUTH")) {
      aS = incomingMessage.get(1).floatValue(); 
      println("aS: " + aS);
    }
    if (microphone.equals("WEST")) {
      aW = incomingMessage.get(1).floatValue(); 
      println("aW: " + aW);
    }
  }
  if (incomingMessage.checkAddrPattern("/clientFreq")) {
    String microphone = incomingMessage.get(0).stringValue();
    if (microphone.equals("FNORTH")) {
      fN = incomingMessage.get(1).floatValue();
    }
    if (microphone.equals("FEAST")) {
      fE = incomingMessage.get(1).floatValue();
    }
    if (microphone.equals("FSOUTH")) {
      fS = incomingMessage.get(1).floatValue();
    }
    if (microphone.equals("FWEST")) {
      fW = incomingMessage.get(1).floatValue();
    }
  }
  incomingMessage.clear(); 
}

public void micCheck() {
  if (aN > 0.1f) {
    player.up = true;
  }
  if (aE > 0.1f) {
    player.right = true;
  }
  if (aS > 0.1f) {
    player.down = true;
  }
  if (aW > 0.1f) {
    player.left = true;
  }
  if (aN < 0.1f) {
    player.up = false;
  }
  if (aE < 0.1f) {
    player.right = false;
  }
  if (aS < 0.1f) {
    player.down = false;
  }
  if (aW < 0.1f) {
    player.left = false;
  }
}

//void keyPressed() {
//  if (keyCode == UP) {
//    player.up = true;
//  } else if (keyCode == DOWN) {
//    player.down = true;
//  } else if (keyCode == LEFT) {
//    player.left = true;
//  } else if (keyCode == RIGHT) {
//    player.right = true;
//  }
//}

//void keyReleased () {
//  if (keyCode == UP) {
//    player.up = false;
//  } else if (keyCode == DOWN) {
//    player.down = false;
//  } else if (keyCode == LEFT) {
//    player.left = false;
//  } else if (keyCode == RIGHT) {
//    player.right = false;
//  }
//}

public void customize(DropdownList ddl) {
  ddl.setBackgroundColor(color(255, 0, 0))
    .setBarHeight(30)
    .setItemHeight(30)
    .setSize(150, 300)
    .setOpen(false); 
  
}

public void resetGame() {
   goal.resetGoal(); 
   for (int i = 0; i < 4; i++) {
     score = 0; 
   }
   
   currentTime = millis();
   startTime = millis();
 }
 
public void keyPressed() {
   if (key == ENTER) {
     resetGame(); 
   }
 }
class Goal {
  float xpos, ypos;
  float size;
  

  Goal (float goalSize) {
    xpos = random(goalSize, width-goalSize);
    ypos = random(goalSize, height-goalSize);
    size = goalSize;
  }
  public void drawGoal () {

    ellipse (xpos, ypos, size, size);
  }
  
  public void resetGoal() {
    xpos = random(goalSize, width-goalSize);
    ypos = random(goalSize, height-goalSize);
  }
}
class Player {
  float xpos, ypos;
  float size;
  Boolean up = false, down = false, right = false, left = false;
  float velocityX = 0, velocityY = 0;

  Player (float playerSize) {
    xpos = random(playerSize, width-playerSize);
    ypos = random(playerSize, height-playerSize);
    size = playerSize;
  }
  
  public void updateSpeed() {
    if (this.left ) { this.velocityX -= a; }
    if (this.right) { this.velocityX += a ; }
    if (this.up) { this.velocityY -= a ;}
    if (this.down) { this.velocityY += a ;}
    
    // Update location.
    this.xpos = (width + this.xpos + this.velocityX) % width;
    this.ypos = (height + this.ypos + this.velocityY) % height;
    // Decrease speed due to traction.
    this.velocityX *= 0.99f;
    this.velocityY *= 0.99f;
  }
  
  public void drawPlayer () {
    fill(0, 255, 0); 
    ellipse (this.xpos, this.ypos, this.size, this.size);
  }
  public Boolean checkOverlap() {
    return dist(this.xpos, this.ypos, goal.xpos, goal.ypos) <= goal.size/2;
  }
}
  public void settings() {  size(1500, 1500); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "server" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
