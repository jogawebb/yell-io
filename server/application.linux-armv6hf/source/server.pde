//Import the relevant libraries. 
import processing.sound.*;
import controlP5.*;

import oscP5.*;
import netP5.*;

//import the font
PFont myFont; 

//Instantiate the audio elements from the Sound library. 
Sound soundCard; 
AudioIn in1, in2; 
Amplitude amp1, amp2; 

//Instantiate osc elements for server-client communication. 
OscP5 oscP5; 
OscProperties properties;  

//Instantiate the game elements from the classes below. 
Player player;
Goal goal;

int score = 0;
int goalSize = 150;
int playerSize = 30;
float a = 0.14;

int currentTime, startTime; 

float aN, aS, aE, aW; 
float fN, fS, fE, fW; 

void setup() {
  Sound.list(); 
  
  myFont = createFont("VT323 Regular", 128); 
  textFont(myFont, 128); 
  
  oscP5 = new OscP5(this, 12000);
  OscProperties properties = new OscProperties(); 
  properties.setDatagramSize(512); 
  
  
  goal = new Goal(goalSize);
  player = new Player(playerSize);
  size(1500, 1500);
  noStroke();
  
  startTime = millis();
  
  exec("/home/gabe/Yell-IO/client1/application.linux64/client1"); 
  exec("/home/gabe/Yell-IO/client2/application.linux64/client2"); 
}

void draw() {

  background(25); 
  fill(255); 

  micCheck(); 
  goal.drawGoal();
  player.drawPlayer();
  player.updateSpeed();

  if (player.checkOverlap()) {
    score++;
    goal.resetGoal();
  }
  textSize(100);
  fill(255); 
  text("Score: " + score, 100, 100); 
  
  currentTime = millis(); 
  int clockTime = currentTime - startTime; 
  
  textSize(100); 
  fill(255); 
  text("Time: " + (60 - (clockTime / 1000)), 800, 100); 

  if (clockTime >= 60000) {
    background(0); 
    text("Congratulations! You scored " + score, width / 4, height / 4, width / 2 + 100, height / 2); 
    text("Press [ENTER] to restart.", width / 4, height / 4 + 500, width / 2, height / 2); 
  
  }
}

void oscEvent(OscMessage incomingMessage) {
  if (incomingMessage.checkAddrPattern("/clientAmp") == true) {
    String microphone = incomingMessage.get(0).stringValue(); 
    if (microphone.equals("NORTH")) {
      aN = incomingMessage.get(1).floatValue(); 
      println("aN: " + aN);
    }
    if (microphone.equals("EAST")) {
      aE = incomingMessage.get(1).floatValue(); 
      println("aE: " + aE);
    }
    if (microphone.equals("SOUTH")) {
      aS = incomingMessage.get(1).floatValue(); 
      println("aS: " + aS);
    }
    if (microphone.equals("WEST")) {
      aW = incomingMessage.get(1).floatValue(); 
      println("aW: " + aW);
    }
  }
  if (incomingMessage.checkAddrPattern("/clientFreq")) {
    String microphone = incomingMessage.get(0).stringValue();
    if (microphone.equals("FNORTH")) {
      fN = incomingMessage.get(1).floatValue();
    }
    if (microphone.equals("FEAST")) {
      fE = incomingMessage.get(1).floatValue();
    }
    if (microphone.equals("FSOUTH")) {
      fS = incomingMessage.get(1).floatValue();
    }
    if (microphone.equals("FWEST")) {
      fW = incomingMessage.get(1).floatValue();
    }
  }
  incomingMessage.clear(); 
}

void micCheck() {
  if (aN > 0.1) {
    player.up = true;
  }
  if (aE > 0.1) {
    player.right = true;
  }
  if (aS > 0.1) {
    player.down = true;
  }
  if (aW > 0.1) {
    player.left = true;
  }
  if (aN < 0.1) {
    player.up = false;
  }
  if (aE < 0.1) {
    player.right = false;
  }
  if (aS < 0.1) {
    player.down = false;
  }
  if (aW < 0.1) {
    player.left = false;
  }
}

//void keyPressed() {
//  if (keyCode == UP) {
//    player.up = true;
//  } else if (keyCode == DOWN) {
//    player.down = true;
//  } else if (keyCode == LEFT) {
//    player.left = true;
//  } else if (keyCode == RIGHT) {
//    player.right = true;
//  }
//}

//void keyReleased () {
//  if (keyCode == UP) {
//    player.up = false;
//  } else if (keyCode == DOWN) {
//    player.down = false;
//  } else if (keyCode == LEFT) {
//    player.left = false;
//  } else if (keyCode == RIGHT) {
//    player.right = false;
//  }
//}

void customize(DropdownList ddl) {
  ddl.setBackgroundColor(color(255, 0, 0))
    .setBarHeight(30)
    .setItemHeight(30)
    .setSize(150, 300)
    .setOpen(false); 
  
}

void resetGame() {
   goal.resetGoal(); 
   for (int i = 0; i < 4; i++) {
     score = 0; 
   }
   
   currentTime = millis();
   startTime = millis();
 }
 
void keyPressed() {
   if (key == ENTER) {
     resetGame(); 
   }
 }
